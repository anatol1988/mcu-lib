#ifndef TEST_DEBOUNCE_H
#define TEST_DEBOUNCE_H

#include <cxxtest/TestSuite.h>

#include <debounce.hpp>

class DebounceTest : public CxxTest::TestSuite
{
public:
    void testDebounce(void)
    {
        mcu::debounce deb(3);

        TS_ASSERT_EQUALS( deb.read(), false );
        deb.update(true);
        TS_ASSERT_EQUALS( deb.read(), false );
        deb.update(true);
        TS_ASSERT_EQUALS( deb.read(), false );
        deb.update(true);
        TS_ASSERT_EQUALS( deb.read(), false );
        deb.update(true);
        TS_ASSERT_EQUALS( deb.read(), true );
        deb.update(true);
        TS_ASSERT_EQUALS( deb.read(), true );
    }
};


#endif // TEST_DEBOUNCE_H
