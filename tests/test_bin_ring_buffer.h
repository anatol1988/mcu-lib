#ifndef TEST_BIN_RING_BUFFER_H
#define TEST_BIN_RING_BUFFER_H

#include <cxxtest/TestSuite.h>

#include <bin_ring_buffer.hpp>

#include <numeric>

#include <iostream>

class BinRingBufferTest : public CxxTest::TestSuite
{
public:
    void testBinRingBuffer()
    {
        mcu::bin_ring_buffer<int, 8> buf;
        buf.push_back(1);
        TS_ASSERT_EQUALS( buf[0], 1 );
        buf.push_back(2);
        TS_ASSERT_EQUALS( buf[1], 2 );
        buf.push_back(3);
        TS_ASSERT_EQUALS( buf[2], 3);
        buf.push_back(4);
        TS_ASSERT_EQUALS( buf[3], 4);
    }

    void testIterator()
    {
        mcu::bin_ring_buffer<int, 8> buf_8;
        mcu::bin_ring_buffer<int, 4> buf_4;

        for (int i = 0; i < 4; ++i) {
            buf_8.push_back(i);
            buf_4.push_back(i);
        }

        buf_4.pop_front();
        buf_4.pop_front();
        buf_4.push_back(10);

        TS_ASSERT_EQUALS( std::accumulate(buf_8.begin(), buf_8.end(), 0), 6 );
        TS_ASSERT_EQUALS( buf_8.back(), 3)
        TS_ASSERT_EQUALS( std::accumulate(buf_4.begin(), buf_4.end(), 0), 15 );
        TS_ASSERT_EQUALS( buf_4.back(), 10)

        mcu::bin_ring_buffer<int, 4> buf_null;
        TS_ASSERT( buf_null.empty());
        TS_ASSERT_EQUALS( std::accumulate(buf_null.begin(), buf_null.end(), 0),
                          0 );
    }
};


#endif // TEST_BIN_RING_BUFFER_H
