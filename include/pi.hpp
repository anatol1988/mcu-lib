#ifndef PI_HPP
#define PI_HPP

#include <assertion.h>
#include <limits>

namespace mcu
{

/// ПИ-регулятор
/**
 *  @note
 *  Реализует простейший anti-windup
 *  http://www.20sim.com/webhelp/library/signal/control/pid_control/antiwindup.htm
 */
template<int DELIM = 8>
class pi
{
public:
    /// Конструктор
    pi(int kp = 0, int ki = 0, int uMax = std::numeric_limits<int>::max(),
        int uMin = std::numeric_limits<int>::max())
    : kp_(kp), ki_(ki), uMax_(uMax), uMin_(uMin)
    {
        reset();
    }
    
    /// Установить значение пропорционального коэффициента
    /**
     * @param kp Пропорциональный коэффициент
     */
    void setKp(int kp)
    {
        kp_ = kp;
    }
    
    /// Пропорциональный коэффициент
    /**
     * @return Пропорциональный коэффициент
     */
    int kp() const
    {
        return kp_;
    }
    
    /// Установить значение интегрального коэффициента
    /**
     * @param ki Интегральный коэффициент
     */
    void setKi(int ki)
    {
        ki_ = ki;
    }
    
    /// Интегральный коэффициент
    /**
     * @return Интегральный коэффициент
     */
    int ki() const
    {
        return ki_;
    }

    /// Задать опорный сигнал
    /**
     * @param ref Опорный сигнал
     */
    void setRef(int ref)
    {
        ref_ = ref;
    }

    /// Получить корректирующий сигнал
    /**
     * @param fbk   Сигнал обратной связи
     * @return      Корректирующий сигнал
     */
    int operator()(int fbk)
    {
        // proportional term
        const int up = ref_ - fbk;
        // integral term
        // anti-windup
        const int ui = out == v1 ? (ki_*up)/DELIM + i1 : 0;
        i1 = ui;

        // control output
        const int v1 = (kp_*(up + ui))/DELIM;

        if (v1 > uMax_)
            out = uMax_;
        else if (v1 < uMin_)
            out = uMin_;
        else
            out = v1;

        return out;
    }

private:
    /// Сбросить текущие настройки регулятора
    void reset()
    {
        ref_ = 0;
        out = 0;
        i1 = 0;
        v1 = 0;
    }

    int ref_;   // Input: reference set-point
    int kp_;    // Parameter: proportional loop gain
    int ki_;    // Parameter: integral gain
    int uMax_;  // Parameter: upper saturation limit
    int uMin_;  // Parameter: lower saturation limit
    int out;    // Output: controller output
    int v1;     // Data: pre-saturated controller output
    int i1;     // Data: integrator storage: ui(k-1)
};

}

#endif // PI_HPP
