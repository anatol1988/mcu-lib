#ifndef BITSET_HPP_
#define BITSET_HPP_

#include <cassert>
#include <stdint.h>

namespace mcu
{

/// Класс для хранения битового множества
/**
 *  Класс-обертка вокруг байта, которая позволяет легко получить доступ к
 *  отдельным его битам.
 *  @tparam N   Количество битов в контейнере
 */
template<unsigned int N>
class bitset
{
public:
    /// Конструктор с инициализирующим значением
    /**
     *  @param set  Инициализирующий байт
     */
    bitset(const char * set)
    {
        for (int i = 0; i < BYTE_NUM; ++i)
            container[i] = *set++;
    }

    /// Конструктор без инициализирующего значения
    bitset()
    {
        for (int i = 0; i < BYTE_NUM; ++i)
            container[i] = 0;
    }

    /// Оператор индексирования
    /**
     *  @param  pos Порядковый номер бита для извлечения
     *  @return     Значение бита
     */
    bool operator[](int pos) const
    {
        assert(pos >= 0 && pos < N)
        const int byteNum = pos/BYTE_SIZE;
        const int posInByte = pos%BYTE_SIZE;
        return container[byteNum] & (1 << posInByte);
    }

    /// Представления содержимого контейнера как массива слов
    /**
     *  @return Указатель на содержимое контейнера
     */
    uint16_t * toWord()
    {
        return container;
    }

private:
    static const unsigned int BYTE_SIZE = 16;
    static const unsigned int BYTE_NUM = (N + BYTE_SIZE - 1)/BYTE_SIZE;
    static const unsigned int BIT_NUM = BYTE_NUM*BYTE_SIZE;

    uint16_t container[BYTE_NUM];
};

}

#endif /* BITSET_HPP_ */
