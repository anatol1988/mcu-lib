#ifndef ENUM_HPP
#define ENUM_HPP

#include <cassert>

namespace mcu
{

/// Шаблон преинкремента перечисления
/**
 *  Применяется для итерирования по перечислениям.
 *  @tparam Enum    Перечисление, для которого производится инкремент
 */
template <class Enum>
Enum& enum_increment(Enum &value, Enum begin, Enum end)
{
    assert(value <= end && value >= begin)
    return value = (value == end) ? begin : static_cast<Enum>(value + 1);
}

}

#endif // ENUM_HPP
