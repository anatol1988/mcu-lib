#ifndef DEBOUNCE_HPP
#define DEBOUNCE_HPP

namespace mcu
{

/// Фильтр дребезга контактов
class debounce
{
public:
    /**
     *  @param[in] threshold Пороговое значение фильтра
     */
    debounce(int threshlod) :
        _threshold(threshlod),
        _counter(0),
        _out(false)
    { }
    
    /**
     *  @param[in] in Значение на вход фильтра
     */
    inline void update(bool in)
    {
        if (in == true) {
            if (_counter < _threshold) {
                _counter++;
            } else {
                _counter = _threshold;
                _out = true;
            }
        } else {
            if (_counter > 0) {
                _counter--;
            } else {
                _counter = 0;
                _out = false;
            }
        }
    }
    
    /**
     * @return Фильтрованное значение цифрового сигнала
     */
    inline bool read() const
    {
        return _out;
    }

private:
    int _counter;
    bool _out;
    const int _threshold;
};

}

#endif // DEBOUNCE_HPP
