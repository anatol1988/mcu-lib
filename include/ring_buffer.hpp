#ifndef RING_BUFFER_HPP
#define RING_BUFFER_HPP

#include <iterator>
#include <assertion.h>

using std::iterator;

namespace mcu
{

/// Кольцевой буфер
/**
 *  @note
 *  Буфер непотокобезопасен
 *  @note
 *  Больше информации о реализациях:
 *  http://en.wikipedia.org/wiki/Circular_buffer.
 *  @tparam T   Тип класса, помещаемого в буфер
 *  @tparam CAP Размер буфера
 */
template<class T, unsigned int CAP>
class ring_buffer
{
public:
    /**
     *  @param initValue    Значение для первоначального заполнения буфера
     */
    ring_buffer(const T &value = T())
    {
        read = 0;
        write = 0;
        count = 0;

        for (int i = 0; i < CAP; ++i)
            _data[i] = value;
    }

    /// Оператор доступа к элементу буфера для чтения
    /**
     *  @param index    Индекс элемента
     *  @return         Элемент кольцевого буфера
     */
    const T & operator[](unsigned int index) const
    {
        assert(index <= CAP);
        return _data[(read + index)%CAP];
    }

    /// Оператор доступа к элементу буфера для записи
    /**
     *  @param index    Индекс элемента
     *  @return         Элемент кольцевого буфера
     */
    T& operator[](unsigned int index)
    {
        assert(index < CAP);
        return _data[(read + index)%CAP];
    }

    /// Включает элемент в буфер
    /**
     *  @param t    Включаемый элемент
     *  @sa         pop_front()
     */
    void push_back(const T &t)
    {
        if (filled())
            pop_front();

        _data[write] = t;
        ++count;

        if (++write >= CAP)
            write = 0;
    }

    /// Возвращает первый элемент из буфера
    /**
     *  @sa push_back()
     */
    void pop_front()
    {
        if (++read >= CAP)
            read = 0;

        --count;
    }

    /// Удаляет последний элемент буфера
    /**
     *  @sa pop_front()
     */
    void pop_back()
    {
        --count;
        write = write == 0 ? CAP - 1 : write - 1;
    }

    /// Проверка буфера на пустоту
    /**
        @retval true    Буфер пуст,
        @retval false   Буфер не пуст
        @sa     filled()
    */
    bool empty() const
    {
        return !count;
    }

    //! Проверка буфера на заполненность
    /**
     *  @retval true    Буфер заполнен,
     *  @retval false   Буфер не заполнен
     *  @sa     empty()
     */
    bool filled() const
    {
        return count == CAP;
    }

    /// Вместимость буфера
    /**
     *  @return Доступное для заполнения место в буфере
     */
    unsigned int capacity() const
    {
        return CAP;
    }

    /// Количество элементов в буфере
    /**
     *  @return Текущее количество элементов в буфере
     */
    unsigned int size() const
    {
        return count;
    }

    /// Доступ к последнему элементу буфера для записи
    /**
     *  @return Ссылка на последний элемент
     */
    T& back()
    {
        const unsigned int index = write != 0 ? write - 1 : CAP - 1;
        return _data[index];
    }

    /// Доступ к последнему элементу буфера для чтения
    /**
     *  @return Константная ссылка на последний элемент
     */
    const T & back() const
    {
        const unsigned int index = write != 0 ? write - 1 : CAP - 1;
        return _data[index];
    }

    /// Доступ к первому элементу буфера для чтения
    /**
     *  @return Ссылка на первый элемент
     */
    const T & front() const
    {
        return _data[read];
    }

    class iterator;
    friend class iterator;
    /// Итератор для значений буфера
    /**
     *  Позволяет применять к буферу алгоритмы стандартной библиотеки
     */
    class iterator : public std::iterator<std::bidirectional_iterator_tag, T>
    {
    public:
        /**
         *  @param rb       Кольцевой буфер
         *  @param element  Элемент, на который указывает итератор
         */
        iterator(ring_buffer<T, CAP> &rb, T &element) : _rb(rb)
        {
            if (_rb.count > 0)
                _element = &element;
            else
                _element = &_rb._data[CAP];
        }

        /**
         *  @param  x       Итератор, с которым происходит сравнение
         *  @retval true    Итераторы указывают на один объект
         *  @retval false   Итераторы указывают на разные объекты
         */
        bool operator==(const iterator &x) const
        {
            return x._element == _element;
        }

        /**
         *  @param  x       Итератор, с которым происходит сравнение
         *  @retval false   Итераторы указывают на один объект
         *  @retval true    Итераторы указывают на разные объекты
         */
        bool operator!=(const iterator &x) const
        {
            return x._element != _element;
        }

        /**
         *  @return Ссылка на связанный объект
         */
        T& operator*()
        {
            return *_element;
        }

        /**
         *  @return Ссылка на следующий по порядку итератор
         */
        iterator& operator++()
        {
            _element++;

            if (_element == &_rb._data[CAP])
                _element = &_rb._data[0];
            else if (_element == &_rb._data[_rb.write])
                _element = &_rb._data[CAP];

            return *this;
        }

        /**
         *  @return Следующий по порядку итератор
         */
        iterator operator++(int)
        {
            iterator tmp = *this;
            ++*this;
            return tmp;
        }

        /**
         *  @return Предыдущий по порядку итератор
         */
        iterator& operator--()
        {
            _element--;
            return *this;
        }

        /**
         *  @return Предыдущий по порядку итератор
         */
        iterator operator--(int)
        {
            iterator tmp = *this;
            --*this;
            return tmp;
        }

    private:
        ring_buffer &_rb;
        T *_element;
    };

    /// Первый элемент буфера
    /**
     *  @return Итератор на первый элемент
     */
    iterator begin()
    {
        T &e = count > 0 ? _data[read] : _data[CAP];
        return iterator(*this, e);
    }

    /// Конец буфера
    /**
     *  @return Итератор на элемент следующий за последним элементом буфера
     */
    iterator end()
    {
        return iterator(*this, _data[CAP]);
    }

private:
    T _data[CAP];
    unsigned int read;
    unsigned int write;
    unsigned int count;
};

}

#endif // RING_BUFFER_HPP
