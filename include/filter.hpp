#ifndef FILTER_HPP
#define FILTER_HPP

namespace mcu
{
/// Интегрирующий фильтр
/**
 *  @tparam     U       Тип большей разрядности, используемый для хранения
 *                      промежуточного результата
 *  @tparam     T       Тип фильтруемых данных
 *  @tparam     CAP     Размер буфера данных
 *  @tparam     Storage Класс, используемый для хранения данных
 *  @param[in]  buffer  Буфер с фильтруемыми данными
 *  @param[in]  window  Размер окна фильтра
 *  @return             Фильтрованное значение
 */
template<class U, class T, size_t CAP,
                                template<class, size_t> class Storage>
T integrate(const Storage<T, CAP> &buffer, size_t window = 0)
{
    const size_t LAST_VALUE = buffer.size() - 1;
    const size_t FIRST_VALUE = window > LAST_VALUE || window == 0 ? 0
                                    : LAST_VALUE - window;
    U mid = 0;

    for (size_t i = LAST_VALUE; i > FIRST_VALUE; --i)
        mid += buffer[i];

    mid /= (LAST_VALUE - FIRST_VALUE);
    return mid;
}


/// Медианный фильтр
/**
 *  @tparam     U       Тип большей разрядности, используемый для хранения
 *                      промежуточного результата
 *  @tparam     T       Тип фильтруемых данных
 *  @tparam     CAP     Размер буфера данных
 *  @tparam     Storage Класс, используемый для хранения данных
 *  @param[in]  buffer  Буфер с фильтруемыми данными
 *  @param[in]  window  Размер окна фильтра
 *  @return             Фильтрованное значение
 */
template<class T, unsigned int size,
                            template<class, unsigned int> class Storage>
T median(const Storage<T, size> &buffer, unsigned int window = 3)
{
    const unsigned int lastValue = size - 1;
    unsigned int n = lastValue;

    if ((buffer[lastValue] <= buffer[lastValue - 1])
            && (buffer[lastValue] <= buffer[lastValue - 2])) {
        n = buffer[lastValue - 1] <= buffer[lastValue - 2]
                                         ? lastValue - 1 : lastValue - 2;
    } else if ((buffer[lastValue - 1] <= buffer[lastValue])
            && (buffer[lastValue - 1] <= buffer[lastValue - 2])) {
        n = buffer[lastValue] <= buffer[lastValue - 2]
                                         ? lastValue : lastValue - 2;
    } else {
        n = buffer[lastValue] <= buffer[lastValue - 1]
                                         ? lastValue : lastValue - 1;
    }

    assert(n < size);

    return buffer[n];
}

}

#endif // FILTER_HPP
