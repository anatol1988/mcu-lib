#ifndef BIN_RING_BUFFER_HPP
#define BIN_RING_BUFFER_HPP

#include <iterator>
#include <cassert>
#include <static_assert.hpp>

using std::iterator;

namespace mcu
{

template<class T, unsigned int CAP>
class bin_ring_buffer
{
private:
    BOOST_STATIC_ASSERT((CAP & (CAP - 1)) == 0);  //SIZE must be a power of 2
    T _data[CAP];
    unsigned int read;
    unsigned int write;
    static const unsigned int MASK = CAP - 1;

public:
    bin_ring_buffer()
    {
        read = 0;
        write = 0;
    }
    
    bool filled() const
    {
        return ((write - read) & static_cast<unsigned int>(~MASK)) != 0;
    }
    
    void push_back(T value)
    {
        assert(!filled());
        _data[write++ & MASK] = value;
    }
    
    void pop_front()
    {
        assert(!empty());
        read++;
    }
    
    const T & front() const
    {
        return _data[read & MASK];
    }
    
    const T & back() const
    {
        return _data[(write - 1) & MASK];
    }
    
    const T & operator [](unsigned int i) const
    {
        assert(!empty());
        return _data[(read + i) & MASK];
    }
    
    bool empty() const
    {
        return write == read;
    }
    
    unsigned int size() const
    {
        return write - read;
    }
    
    void clear()
    {
        read = 0;
        write = 0;
    }
    
    unsigned int capacity()
    {
        return CAP;
    }

    /// Итератор для значений буфера
    /**
     *  Позволяет применять к буферу алгоритмы стандартной библиотеки
     */
    class iterator;
    friend class iterator;
    /// Итератор для значений буфера
    /**
     *  Позволяет применять к буферу алгоритмы стандартной библиотеки
     */
    class iterator : public std::iterator<std::bidirectional_iterator_tag, T>
    {
    public:
        /**
         *  @param rb       Кольцевой буфер
         *  @param element  Элемент, на который указывает итератор
         */
        iterator(bin_ring_buffer<T, CAP> &rb, T &element) : _rb(rb)
        {
            if (_rb.size() > 0)
                _element = &element;
            else
                _element = &_rb._data[CAP];
        }

        /**
         *  @param  x       Итератор, с которым происходит сравнение
         *  @retval true    Итераторы указывают на один объект
         *  @retval false   Итераторы указывают на разные объекты
         */
        bool operator==(const iterator &x) const
        {
            return x._element == _element;
        }

        /**
         *  @param  x       Итератор, с которым происходит сравнение
         *  @retval false   Итераторы указывают на один объект
         *  @retval true    Итераторы указывают на разные объекты
         */
        bool operator!=(const iterator &x) const
        {
            return x._element != _element;
        }

        /**
         *  @return Ссылка на связанный объект
         */
        T& operator*()
        {
            return *_element;
        }

        /**
         *  @return Ссылка на следующий по порядку итератор
         */
        iterator& operator++()
        {
            _element++;

            if (_element == &_rb._data[CAP])
                _element = &_rb._data[0];
            else if (_element == &_rb._data[_rb.write & MASK])
                _element = &_rb._data[CAP];

            return *this;
        }

        /**
         *  @return Следующий по порядку итератор
         */
        iterator operator++(int)
        {
            iterator tmp = *this;
            ++*this;
            return tmp;
        }
        /**
         *  @return Предыдущий по порядку итератор
         */
        iterator& operator--()
        {
            _element--;
            return *this;
        }

        /**
         *  @return Предыдущий по порядку итератор
         */
        iterator operator--(int)
        {
            iterator tmp = *this;
            --*this;
            return tmp;
        }

    private:
        bin_ring_buffer &_rb;
        T *_element;
    };

    /// Первый элемент буфера
    /**
     *  @return Итератор на первый элемент
     */
    iterator begin()
    {
        T &e = size() ? _data[read] : _data[CAP];
        return iterator(*this, e);
    }

    /// Конец буфера
    /**
     *  @return Итератор на элемент следующий за последним элементом буфера
     */
    iterator end()
    {
        return iterator(*this, _data[CAP]);
    }
};

}

#endif // BIN_RING_BUFFER_HPP
